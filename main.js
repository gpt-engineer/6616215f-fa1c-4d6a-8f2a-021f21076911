// Game state
let board = ["", "", "", "", "", "", "", "", ""];
let currentPlayer = "X";
let gameActive = true;

// Winning combinations
const winningCombinations = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

// DOM elements
const cells = document.querySelectorAll(".cell");
const message = document.getElementById("message");
const playAgainButton = document.getElementById("play-again");

// Function to handle cell click
function handleCellClick(index) {
  if (!gameActive || board[index] !== "") {
    return;
  }

  board[index] = currentPlayer;
  cells[index].textContent = currentPlayer;
  cells[index].classList.add(currentPlayer === "X" ? "text-blue-500" : "text-red-500");

  if (checkWin()) {
    endGame(false);
  } else if (isBoardFull()) {
    endGame(true);
  } else {
    currentPlayer = currentPlayer === "X" ? "O" : "X";
  }
}

// Function to check for a win
function checkWin() {
  for (let i = 0; i < winningCombinations.length; i++) {
    const [a, b, c] = winningCombinations[i];
    if (board[a] !== "" && board[a] === board[b] && board[a] === board[c]) {
      return true;
    }
  }
  return false;
}

// Function to check if the board is full
function isBoardFull() {
  return board.every((cell) => cell !== "");
}

// Function to end the game
function endGame(isTie) {
  gameActive = false;
  message.textContent = isTie ? "It's a tie!" : `Player ${currentPlayer} wins!`;
  playAgainButton.style.display = "block";
}

// Function to reset the game
function resetGame() {
  board = ["", "", "", "", "", "", "", "", ""];
  currentPlayer = "X";
  gameActive = true;
  message.textContent = "";
  playAgainButton.style.display = "none";
  cells.forEach((cell) => {
    cell.textContent = "";
    cell.classList.remove("text-blue-500", "text-red-500");
  });
}

// Add event listeners to cells
cells.forEach((cell, index) => {
  cell.addEventListener("click", () => handleCellClick(index));
});

// Add event listener to play again button
playAgainButton.addEventListener("click", resetGame);
